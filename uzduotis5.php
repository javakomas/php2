<html>

<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	 crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	 crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	 crossorigin="anonymous"></script>
</head>

<body>
	<div class="container">
		<h1>Viktorina</h1>
		<p>
			<?php
if (!empty($_POST)) {
    $correct = 0;
    if (!empty($_POST['question_1']) && $_POST['question_1'] == 1) {
        $correct++;
    }

    if (!empty($_POST['question_2']) && $_POST['question_2'] == 4) {
        $correct++;
    }

    if (!empty($_POST['question_3']) && $_POST['question_3'] == 2) {
        $correct++;
    }

    $correctPercent = 100 * round($correct / 3, 2);
    echo "Teisingai atsakėte $correctPercent %.";
} else {
    echo 'Pasirinkite atsakymus.';
}
?>
		</p>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<i class="icon-calendar"></i>
						<h3 class="panel-title">Pažymėkite teisingus atsakymus</h3>
					</div>
					<div class="panel-body">
						<form class="form-horizontal row-border" method="POST">
							<div class="form-group">
								<div class="col-md-2">
									<img src="ans1.png" width="150" />
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_1" value="1"> Ančiasnapis
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_1" value="2"> Antis
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_1" value="3"> Elnias
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_1" value="4"> Šuo
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-2">
									<img src="dog.jpg" width="150" />
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_2" value="1"> Ančiasnapis
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_2" value="2"> Antis
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_2" value="3"> Elnias
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_2" value="4"> Šuo
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-2">
									<img src="duck.jpg" width="150" />
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_3" value="1"> Ančiasnapis
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_3" value="2"> Antis
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_3" value="3"> Elnias
								</div>
								<div class="col-md-10">
									<input type="radio" name="question_3" value="4"> Šuo
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label"></label>
								<div class="col-md-10">
									<input type="submit" value="Siųsti" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</body>

</html>
