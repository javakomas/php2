PHP-2 - Sąlyginiai sakiniai

Boolean kintamieji

Gali įgauti vieną iš dviejų reikšmių - true, false

    $loggedIn = false;
    $isAdmin = false;
    $agreedToTermsAndConditions = true;


Kintamojo reikšmės ir tipo sužinojimas

var_dump($result);


If'as


Sąlyginiam sakiniam konstruoti naudojami žodžiai if, else, elseif (arba else if). 


$amount = 100; //įvesta suma nusiimti pinigų iš bankomato

$balance = 50; //banko kortelės pinigų suma

$loanSelected = true; //pasirinkta imti paskolą


Pavyzdys 1:

if ($amount > $balance ) {
    echo 'Nepakanka lėšų!';
}


Pavyzdys 2:

if ($amount <= $balance) {
    echo 'Pasiimkite pinigus';
    $balance = $balance - $amount;
} else {
    echo 'Nepakanka lėšų operacijai atlikti!'
}


Pavyzdys 3:

if ($amount <= $balance) {
    echo 'Pasiimkite pinigus';
    $balance = $balance - $amount;
} elseif ($loanSelected === true) {
    $balance = $balance - $amount;
    echo 'Pavyko sėkmingai pasiskolinti pinigų. Passimkite pinigus';
} else {
    echo 'Nepakanka lėšų operacijai atlikti!';
}

http://php.net/manual/en/control-structures.if.php


Switch'as

Jeigu reikia tikrinti daugiau negu 2 sąlygas tam pačiam kintamajam, 
rekomenduojama naudoti switch struktūrą

Pavyzdys 1 

//blogas pavyzdys

if ($operation === 1) {
    $message = 'Pasirinkote peržiūrėti sąskaitą';
} elseif ($operation === 2) {
    $message = 'Pasirinkote nuimti pinigus';
} elseif ($operation === 3) {
    $message = 'Pasirinkote išeiti';
} else {
    $message = 'Nežinoma operacija';
}


Pavyzdys 2

//geras pavyzdys

switch ($operation) {
    case 1:
        $message = 'Pasirinkote peržiūrėti sąskaitą';
    break;
    case 2:
        $message = 'Pasirinkote nuimti pinigus';
    break;
    case 3:
        $message = 'Pasirinkote išeiti';

    break;
    default: 
        $message = 'Nežinoma operacija';
    break;
}

http://php.net/manual/en/control-structures.switch.php

If'o trumpinys - "If shorthand"

if ($age >= 18) {
    $licenseType = 'full';
} else {
    $licenseType = 'limited';
}

galima pakeisti į:

$licenseType = $age >= 18 ? 'full' : 'limited';


Palyginimo operatoriai

Palygina reikšmes, net jei jos skirtingo tipo:

    $a == $b
    $a != $b
    $a <> $b

    $a < $b
    $a <= $b
    $a > $b
    $a >= $b

Palygina reikšmes ir kintamųjų tipus:

    $a === $b
    $a !== $b

http://php.net/manual/en/language.operators.comparison.php
Spaceship operatorius

    5 <=> 7 = -1
    5 <=> 5 = 0
    5 <=> 4 = 1 


Null coalesce operatorius

$username = $_GET['user'] ?? 'nobody';


Operacijų rezultatų tipai

    Vykdant aritmetines operacijas ('+', '-', '%', '/', '++', '*=', ..),
     rezultatas visada yra "integer" arba "float" tipo, pvz:  '03' * '004.2' = 12.6
    Vykdant "string" operacijas ('.', '.='), rezultatas visada yra "string" tipo,
     pvz:    3 . 3.11 = '33.11'
    Rezultatas visada yra bool tipo kai vykdamos palyginimo,
     loginės operacijos ar neigimasl, išskyrus <==> : ( &&, ||, !, ==, ===, !=, !==, <, >, <=),
      pvz:  'text' || 1 = true
    <==> operatoriaus rezultatas yra 0, 1, arba -1

http://php.net/manual/en/types.comparisons.php


Kintamųjų tipų lankstumas - Type juggling

Kai vykdamos loginės operacijos, kintamieji paverčiami į boolean tipą (tai įvyksta slaptai)

if ($name) { //...

slaptai įvykdo

if ((bool) $name) {//...

!$a //...

slaptai įvykdo !((bool) $a)



Dėmesio

    (bool) 'false' = true
    (bool) '0' = false
    (bool) [] = false
    ("1" == "01) = true
    ( "10" == "1e1" ) = true
    (100 == "1e2") = true


empty, isset

http://php.net/manual/en/function.empty.php

http://php.net/manual/en/function.isset.php

if (!empty($_POST['message'])) {
//...
}

if ($_POST['age'] > 18) { //... crash'ina šita eilutė, jeigu 'age' nebuvo nusiųstas
}


reiktų naudoti

if (isset($_POST['age']) && $_POST['age'] > 18) {
}


Boolean logikos formulės

    true && true = true
    true || true = true


    false && false = false
    false || false = false 


    true || false = true
    true && false = false

    $a || true = true
    $a || false = (bool) $a


    $a && false = false
    $a && true = (bool) $a


    !!$a = (bool) $a


    ($a && $b) && $c = $a && ($b && $c) = $a && $b && $c
    !($a && $b && $c) = !$a || !$b || !$c
    !($a || $b || $c) = !$a && !$b && !c
    $a && ($b || $c) = ($a && $b) || ($a && $c)